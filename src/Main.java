import java.io.IOException;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import static java.lang.Integer.parseInt;

public class Main {
    public static void main(String[] args) {

        String num1;
        String num2;
        Scanner scanner = new Scanner(System.in);
        System.out.println("enter num1: ");
        num1 = scanner.next();
        System.out.println("enter num2: ");
        num2 = scanner.next();
        Logger logger = Logger.getLogger("MyLog");
        FileHandler fh;

        try {

            fh = new FileHandler("MyLogFile.log");
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);

            if(num2.length()<num1.length()){
                System.out.println();
                logger.info("result sum "+num1+" and "+num2+ " = " +MyBigNumber.sumBigNum(num1,num2));
            }else {
                logger.info("result sum "+num1+" and "+num2+ " = " +MyBigNumber.sumBigNum(num2,num1));
            }


        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}