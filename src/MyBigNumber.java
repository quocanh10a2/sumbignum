import static java.lang.Integer.parseInt;

public class MyBigNumber {
    public static String sumBigNum(String num1,String num2){
        Integer x = num1.length();
        Integer y = num2.length();
        char[] arr1 = num1.toCharArray();
        char[] arr2 = num2.toCharArray();
        StringBuilder sb1 = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        StringBuilder sum = new StringBuilder();
        Integer cache = 0;
        for(char i : arr1){
            sb1.append(i);
        }
        for(char i : arr2){
            sb2.append(i);
        }
        if(x>y){
            for(int i =0;i<x-y;i++){
                sb2.insert(0,"0");
            }
        }

        for (int i =x-1;i>=0;i--){
            Integer result = parseInt(String.valueOf((sb1.charAt(i))))+parseInt(String.valueOf(sb2.charAt(i)))+cache;
            cache = result/10;
            Integer fin = result % 10;
            sum.append(fin.toString().charAt(0));
        }
        if (cache==1) {
            sum.append("1");
        }
        sum.reverse();

        return sum.toString();
    }
}
